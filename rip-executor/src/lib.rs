#![no_std]

use action::{Action, ActionGenerator, Instant, Progress, Prolonged};
use core::time::Duration;
use rip_kinematics::kinematics::{Forward, Inverse, Kinematics};

pub mod action;

pub trait RobotConfig {
    type Generalized: Clone;
    type Position: Clone;
    type Kinematics: Kinematics<Generalized = Self::Generalized, Position = Self::Position>
        + Forward
        + Inverse;
}

pub trait ExecutorConfig: RobotConfig + Sized {
    type Prolonged: Prolonged<Self>;
    type Instant: Instant<Self>;
    type Generator: ActionGenerator<Self>;
    type Error;

    type SharedState: SharedState;
    type SpecificationSource: SpecificationSource<Self>;
}

pub trait SharedState {
    type VelocityModel;
    fn update(&mut self, delta: Duration);
    fn velocity_model(&self) -> &Self::VelocityModel;
}

pub enum ExecutorProgress {
    Working,
    NoNextAction,
}

pub trait SpecificationSource<Cfg>
where
    Cfg: ExecutorConfig,
{
    fn try_produce_specification(&mut self) -> Option<Cfg::Generator>;
}

pub struct Executor<Cfg>
where
    Cfg: ExecutorConfig,
{
    generalized: Cfg::Generalized,
    current_action: Option<Action<Cfg>>,
    next_action_generator: Option<Cfg::Generator>,
    spec_source: Cfg::SpecificationSource,
    kinematics: Cfg::Kinematics,
    fk_spec: <Cfg::Kinematics as Kinematics>::FkSpecification,
    shared_state: Cfg::SharedState,
}

impl<Cfg> Executor<Cfg>
where
    Cfg: ExecutorConfig,
{
    pub fn new(
        generalized: Cfg::Generalized,
        shared_state: Cfg::SharedState,
        kinematics: Cfg::Kinematics,
        spec_source: Cfg::SpecificationSource,
        fk_spec: <Cfg::Kinematics as Kinematics>::FkSpecification,
    ) -> Self {
        let current_action = None;
        let next_action_generator = None;
        Self {
            generalized,
            current_action,
            next_action_generator,
            spec_source,
            kinematics,
            fk_spec,
            shared_state,
        }
    }

    pub fn step(&mut self, delta: Duration) -> Result<ExecutorProgress, Cfg::Error> {
        self.shared_state.update(delta);

        if self.next_action_generator.is_none() {
            self.next_action_generator = self.spec_source.try_produce_specification();
        }

        if self.current_action.is_none() {
            if let Some(next_action_generator) = self.next_action_generator.take() {
                if let Some(motion) = next_action_generator.try_convert(self) {
                    self.current_action = Some(motion);
                }
            }
        }

        let action = self.current_action.take();
        if let Some(action) = action {
            if let Err(err) = match action {
                Action::Instant(instant) => instant.execute(self),
                Action::Prolonged(prolonged) => match prolonged.step(self, delta) {
                    Ok(progress) => {
                        if let Progress::Pending(action) = progress {
                            self.current_action = Some(Action::Prolonged(action))
                        }
                        Ok(())
                    }
                    Err(err) => Err(err),
                },
            } {
                Err(err)
            } else {
                Ok(ExecutorProgress::Working)
            }
        } else {
            Ok(ExecutorProgress::NoNextAction)
        }
    }

    pub fn kinematics(&self) -> &Cfg::Kinematics {
        &self.kinematics
    }

    pub fn shared_state(&self) -> &Cfg::SharedState {
        &self.shared_state
    }

    pub fn shared_state_mut(&mut self) -> &mut Cfg::SharedState {
        &mut self.shared_state
    }

    pub fn generalized(&self) -> &Cfg::Generalized {
        &self.generalized
    }

    pub fn set_generalized(&mut self, generalized: Cfg::Generalized) {
        self.generalized = generalized;
    }

    pub fn fk_spec(&self) -> &<Cfg::Kinematics as Kinematics>::FkSpecification {
        &self.fk_spec
    }
}
