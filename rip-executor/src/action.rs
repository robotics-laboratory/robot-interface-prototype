use crate::{Executor, ExecutorConfig};
use core::time::Duration;

pub enum Action<Cfg>
where
    Cfg: ExecutorConfig,
{
    Prolonged(Cfg::Prolonged),
    Instant(Cfg::Instant),
}

pub trait ActionGenerator<Cfg>
where
    Cfg: ExecutorConfig,
{
    fn try_convert(self, executor: &Executor<Cfg>) -> Option<Action<Cfg>>;
}

#[derive(Debug)]
pub enum Progress<S> {
    Pending(S),
    Finished,
}

pub trait Prolonged<Cfg>: Sized
where
    Cfg: ExecutorConfig,
{
    fn step(
        self,
        executor: &mut Executor<Cfg>,
        delta: Duration,
    ) -> Result<Progress<Self>, Cfg::Error>;
}

impl<Cfg> Prolonged<Cfg> for ()
where
    Cfg: ExecutorConfig,
{
    fn step(self, _: &mut Executor<Cfg>, _: Duration) -> Result<Progress<Self>, Cfg::Error> {
        Ok(Progress::Finished)
    }
}

pub trait Instant<Cfg>
where
    Cfg: ExecutorConfig,
{
    fn execute(self, executor: &mut Executor<Cfg>) -> Result<(), Cfg::Error>;
}

impl<Cfg> Instant<Cfg> for ()
where
    Cfg: ExecutorConfig,
{
    fn execute(self, _: &mut Executor<Cfg>) -> Result<(), Cfg::Error> {
        Ok(())
    }
}
