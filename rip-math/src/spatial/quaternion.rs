use super::vector::Vector;
use crate::{unit::angular::Radian, util};
use core::ops::{Add, Mul, Neg, Sub};
use num::{traits::FloatConst, Float, One, Zero};

/// Spatial rotation representation.
#[derive(Clone, Copy, Debug)]
pub struct Quaternion<N> {
    w: N,
    x: N,
    y: N,
    z: N,
}

impl<N> Quaternion<N> {
    pub fn with_wxyz(w: N, x: N, y: N, z: N) -> Self {
        Quaternion { w, x, y, z }
    }
}

impl<N> Quaternion<N>
where
    N: One + Zero,
{
    pub fn identity() -> Self {
        Quaternion {
            w: N::one(),
            x: N::zero(),
            y: N::zero(),
            z: N::zero(),
        }
    }
}
impl<N> Quaternion<N> {
    pub fn w(&self) -> &N {
        &self.w
    }

    pub fn w_mut(&mut self) -> &mut N {
        &mut self.w
    }

    pub fn x(&self) -> &N {
        &self.x
    }

    pub fn x_mut(&mut self) -> &mut N {
        &mut self.x
    }

    pub fn y(&self) -> &N {
        &self.y
    }

    pub fn y_mut(&mut self) -> &mut N {
        &mut self.y
    }

    pub fn z(&self) -> &N {
        &self.z
    }

    pub fn z_mut(&mut self) -> &mut N {
        &mut self.z
    }
}

impl<N> Quaternion<N>
where
    N: Float,
{
    /// Construct quaternion from given rotation axis and angle.
    ///
    /// # Panics
    /// If `axis.magnitude().iz_zero()`
    pub fn from_angle_axis_unchecked(angle: Radian<N>, axis: Vector<N>) -> Self {
        let half = angle.0 / (N::one() + N::one());
        let cos = half.cos();
        let sin = half.sin();
        let xyz = axis.unit().unwrap() * sin;
        Quaternion {
            w: cos,
            x: *xyz.x(),
            y: *xyz.y(),
            z: *xyz.z(),
        }
    }

    pub fn from_angle_axis(angle: Radian<N>, axis: Vector<N>) -> Option<Self> {
        let half = angle.0 / (N::one() + N::one());
        let cos = half.cos();
        let sin = half.sin();
        let xyz = axis.unit()? * sin;
        Some(Quaternion {
            w: cos,
            x: *xyz.x(),
            y: *xyz.y(),
            z: *xyz.z(),
        })
    }
}

impl<N> Quaternion<N>
where
    N: Neg<Output = N>,
{
    pub fn conjugate(self) -> Self {
        Quaternion::with_wxyz(self.w, -self.x, -self.y, -self.z)
    }
}

impl<N> Quaternion<N>
where
    N: Add<Output = N> + Sub<Output = N> + Mul<Output = N> + Copy,
{
    pub fn cross(&self, other: &Self) -> Self {
        Quaternion::with_wxyz(
            self.w * other.w - self.x * other.x - self.y * other.y - self.z * other.z,
            self.w * other.x + self.x * other.w + self.y * other.z - self.z * other.y,
            self.w * other.y - self.x * other.z + self.y * other.w + self.z * other.x,
            self.w * other.z + self.x * other.y - self.y * other.x + self.z * other.w,
        )
    }

    pub fn dot(&self, other: &Self) -> N {
        self.w * other.w + self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub fn times(&self, other: N) -> Self {
        Quaternion::with_wxyz(
            self.w * other,
            self.x * other,
            self.y * other,
            self.z * other,
        )
    }

    pub fn plus(&self, other: &Self) -> Self {
        Quaternion::with_wxyz(
            self.w + other.w,
            self.x + other.x,
            self.y + other.y,
            self.z + other.z,
        )
    }

    pub fn minus(&self, other: &Self) -> Self {
        Quaternion::with_wxyz(
            self.w - other.w,
            self.x - other.x,
            self.y - other.y,
            self.z - other.z,
        )
    }
}
impl<N> Quaternion<N>
where
    N: Float,
{
    pub fn magnitude(&self) -> N {
        N::sqrt(self.w.powi(2) + self.x.powi(2) + self.y.powi(2) + self.z.powi(2))
    }

    pub fn unit(&self) -> Option<Self> {
        let magnitude = self.magnitude();
        if magnitude.is_zero() {
            None
        } else {
            Some(self.times(N::one() / magnitude))
        }
    }

    pub fn slerp(from: &Self, to: &Self, t: N) -> Self {
        let mut from = *from;
        let mut dot = from.dot(to);
        if dot.is_sign_negative() {
            from = Quaternion::with_wxyz(-from.w, -from.x, -from.y, -from.z);
            dot = -dot;
        }
        if dot >= N::one() - N::epsilon() {
            from
        } else if let Some(q) = (*to - from * dot).unit() {
            let theta = dot.acos() * t;
            from * theta.cos() + q * theta.sin()
        } else {
            from
        }
    }

    pub fn rotate<T: Into<Vector<N>>>(&self, vector: T) -> Vector<N> {
        let vector = vector.into();
        let pure = Quaternion::with_wxyz(N::zero(), *vector.x(), *vector.y(), *vector.z());
        let pure_rotated = *self * pure * self.conjugate();
        Vector::with_xyz(*pure_rotated.x(), *pure_rotated.y(), *pure_rotated.z())
    }

    pub fn delta(&self, other: &Self) -> Quaternion<N> {
        self.conjugate() * *other
    }

    pub fn euler_zyx(&self) -> Option<(Radian<N>, Radian<N>, Radian<N>)> {
        let unit = self.unit()?;
        let two = N::one() + N::one();
        let a = N::atan2(
            two * (unit.w * unit.z + unit.x * unit.y),
            N::one() - two * (unit.y * unit.y + unit.z * unit.z),
        );
        let b = N::asin(two * (unit.w * unit.y - unit.x * unit.z));
        let c = N::atan2(
            two * (unit.w * unit.x + unit.y * unit.z),
            N::one() - two * (unit.x * unit.x + unit.y * unit.y),
        );
        Some((Radian(a), Radian(b), Radian(c)))
    }
}

impl<N> Quaternion<N>
where
    N: Float + FloatConst,
{
    pub fn stored_angle(&self) -> Option<Radian<N>> {
        let two = N::one() + N::one();
        let unit = self.unit()?;
        let angle = two * N::acos(unit.w);
        let angle = util::wrap_value_from_to(angle, -N::PI(), N::PI()).abs();
        Some(Radian(angle))
    }

    pub fn stored_angle_axis(&self) -> Option<(Radian<N>, Vector<N>)> {
        let two = N::one() + N::one();
        let unit = self.unit()?;
        let half_angle = N::acos(unit.w);
        if half_angle.is_zero() {
            return None;
        }
        let axis = Vector::with_xyz(self.x, self.y, self.z) * (N::one() / N::sin(half_angle));
        let angle = two * half_angle;
        let angle = util::wrap_value_from_to(angle, -N::PI(), N::PI());
        if angle.is_sign_negative() {
            Some((Radian(-angle), -axis))
        } else {
            Some((Radian(angle), axis))
        }
    }

    pub fn angle_between(&self, other: &Self) -> Option<Radian<N>> {
        self.delta(other).stored_angle()
    }
}

impl<N> Mul for Quaternion<N>
where
    N: Float,
{
    type Output = Quaternion<N>;
    fn mul(self, rhs: Quaternion<N>) -> Self::Output {
        self.cross(&rhs)
    }
}

impl<N> Mul<Vector<N>> for Quaternion<N>
where
    N: Float,
{
    type Output = Vector<N>;
    fn mul(self, rhs: Vector<N>) -> Self::Output {
        self.rotate(rhs)
    }
}

impl<N> Mul<N> for Quaternion<N>
where
    N: Float,
{
    type Output = Quaternion<N>;
    fn mul(self, rhs: N) -> Self::Output {
        self.times(rhs)
    }
}

impl<N> Add<Quaternion<N>> for Quaternion<N>
where
    N: Float,
{
    type Output = Quaternion<N>;
    fn add(self, rhs: Quaternion<N>) -> Self::Output {
        self.plus(&rhs)
    }
}

impl<N> Sub<Quaternion<N>> for Quaternion<N>
where
    N: Float,
{
    type Output = Quaternion<N>;
    fn sub(self, rhs: Quaternion<N>) -> Self::Output {
        self.minus(&rhs)
    }
}

impl<N> From<(N, N, N, N)> for Quaternion<N> {
    fn from(source: (N, N, N, N)) -> Self {
        Self::with_wxyz(source.0, source.1, source.2, source.3)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::unit::angular::DEG;

    const EPSILON: f64 = 1e-6;

    #[test]
    #[should_panic]
    fn unchecked_generation_panics_on_zero_vector() {
        Quaternion::from_angle_axis_unchecked(Radian(0.0), Vector::with_xyz(0.0, 0.0, 0.0));
    }

    #[test]
    fn generation_is_predictable_on_zero_vector() {
        assert!(
            Quaternion::from_angle_axis(Radian(0.0), Vector::with_xyz(0.0, 0.0, 0.0)).is_none()
        );
        assert!(
            Quaternion::from_angle_axis(Radian(0.0), Vector::with_xyz(-0.0, -0.0, -0.0)).is_none()
        );
        assert!(
            Quaternion::from_angle_axis(Radian(0.0), Vector::with_xyz(1.0, 0.0, 0.0)).is_some()
        );
    }

    #[test]
    fn angle_is_calculated_properly() {
        let angle = Radian(1.0);
        let q = Quaternion::from_angle_axis(angle, Vector::with_xyz(1.0, 1.0, 1.0)).unwrap();
        let calculated = q.stored_angle().unwrap();
        assert!((angle - calculated).0.abs() < EPSILON);

        let angle = Radian(2.0);
        let q = Quaternion::from_angle_axis(angle, Vector::with_xyz(1.0, 1.0, 1.0)).unwrap();
        let calculated = q.stored_angle().unwrap();
        assert!((angle - calculated).0.abs() < EPSILON);
    }

    #[test]
    fn angle_axis_are_calculated_properly() {
        let angle = Radian(1.0);
        let axis = Vector::with_xyz(1.0, 2.0, 3.0);
        let q = Quaternion::from_angle_axis(angle, axis).unwrap();
        let (calculated_angle, calculated_axis) = q.stored_angle_axis().unwrap();
        assert!((angle - calculated_angle).0.abs() < EPSILON);
        assert!((axis.unit().unwrap() - calculated_axis).magnitude() < EPSILON);

        let angle = Radian(2.0);
        let axis = Vector::with_xyz(-1.0, -2.0, -3.0);
        let q = Quaternion::from_angle_axis(angle, axis).unwrap();
        let (calculated_angle, calculated_axis) = q.stored_angle_axis().unwrap();
        assert!((angle - calculated_angle).0.abs() < EPSILON);
        assert!((axis.unit().unwrap() - calculated_axis).magnitude() < EPSILON);
    }

    #[test]
    fn euler_zyx_is_proper() {
        let (a, b, c) = (10.0 * DEG, 15.0 * DEG, 25.0 * DEG);
        let q = Quaternion::from_angle_axis_unchecked(a, Vector::unit_z())
            * Quaternion::from_angle_axis_unchecked(b, Vector::unit_y())
            * Quaternion::from_angle_axis_unchecked(c, Vector::unit_x());

        let (test_a, test_b, test_c) = q.euler_zyx().unwrap();
        assert!((a.0 - test_a.0).abs() < EPSILON);
        assert!((b.0 - test_b.0).abs() < EPSILON);
        assert!((c.0 - test_c.0).abs() < EPSILON);
    }
}
