use super::{quaternion::Quaternion, vector::Vector};
use core::ops::Add;
use num::{Float, One, Zero};

/// Spatial translation-rotation representation.
#[derive(Clone, Copy, Debug)]
pub struct Transform<N> {
    translation: Vector<N>,
    rotation: Quaternion<N>,
}

impl<N> Transform<N> {
    pub fn with_translation_rotation<V: Into<Vector<N>>, Q: Into<Quaternion<N>>>(
        translation: V,
        rotation: Q,
    ) -> Self {
        Transform {
            translation: translation.into(),
            rotation: rotation.into(),
        }
    }
}
impl<N> Transform<N>
where
    N: One + Zero,
{
    pub fn identity() -> Self {
        Transform {
            translation: Vector::<N>::zero(),
            rotation: Quaternion::identity(),
        }
    }

    pub fn translation(&self) -> &Vector<N> {
        &self.translation
    }

    pub fn translation_mut(&mut self) -> &mut Vector<N> {
        &mut self.translation
    }

    pub fn rotation(&self) -> &Quaternion<N> {
        &self.rotation
    }

    pub fn rotation_mut(&mut self) -> &mut Quaternion<N> {
        &mut self.rotation
    }
}
impl<N> Transform<N>
where
    N: Float,
{
    pub fn combine(&self, other: Self) -> Self {
        Transform::with_translation_rotation(
            self.translation + self.rotation * other.translation,
            self.rotation * other.rotation,
        )
    }

    pub fn inverse(&self) -> Self {
        let inverse_rotation = self.rotation.conjugate();
        Transform::with_translation_rotation(
            inverse_rotation * (self.translation * -N::one()),
            inverse_rotation,
        )
    }

    pub fn apply<T: Into<Vector<N>>>(&self, vector: T) -> Vector<N> {
        self.translation + self.rotation * vector.into()
    }

    pub fn lerp(from: &Self, to: &Self, t: N) -> Self {
        Transform::with_translation_rotation(
            Vector::lerp(&from.translation, &to.translation, t),
            Quaternion::slerp(&from.rotation, &to.rotation, t),
        )
    }

    pub fn delta(&self, other: &Self) -> Self {
        self.inverse() + *other
    }
}

impl<N> Add<Transform<N>> for Transform<N>
where
    N: Float,
{
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        self.combine(rhs)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn creates_transform_from_tuples() {
        let _transform =
            Transform::with_translation_rotation((0.0, 1.0, 2.0), (0.0, 0.0, 1.0, 0.0));
    }

    #[test]
    fn transforms_tuple() {
        let transform = Transform::with_translation_rotation((0.0, 1.0, 2.0), (0.0, 1.0, 0.0, 0.0));
        transform.apply((1.0, 2.0, 3.0));
    }
}
