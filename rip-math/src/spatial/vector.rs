use crate::unit::angular::Radian;
use core::ops::{Add, Mul, Neg, Sub};
use num::{Float, One, Zero};

/// Spatial vector for translation representation.
#[derive(Clone, Copy, Debug)]
pub struct Vector<N> {
    x: N,
    y: N,
    z: N,
}

impl<N> Vector<N> {
    pub fn with_xyz(x: N, y: N, z: N) -> Self {
        Vector { x, y, z }
    }
}

impl<N> Vector<N>
where
    N: Zero,
{
    pub fn zero() -> Self {
        Vector {
            x: N::zero(),
            y: N::zero(),
            z: N::zero(),
        }
    }
}

impl<N> Vector<N>
where
    N: Zero + One,
{
    pub fn unit_x() -> Self {
        Vector {
            x: N::one(),
            y: N::zero(),
            z: N::zero(),
        }
    }

    pub fn unit_y() -> Self {
        Vector {
            x: N::zero(),
            y: N::one(),
            z: N::zero(),
        }
    }

    pub fn unit_z() -> Self {
        Vector {
            x: N::zero(),
            y: N::zero(),
            z: N::one(),
        }
    }
}

impl<N> Vector<N> {
    pub fn x(&self) -> &N {
        &self.x
    }

    pub fn x_mut(&mut self) -> &mut N {
        &mut self.x
    }

    pub fn y(&self) -> &N {
        &self.y
    }

    pub fn y_mut(&mut self) -> &mut N {
        &mut self.y
    }

    pub fn z(&self) -> &N {
        &self.z
    }

    pub fn z_mut(&mut self) -> &mut N {
        &mut self.z
    }
}

impl<N> Vector<N>
where
    N: Float,
{
    pub fn magnitude(&self) -> N {
        N::sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    }

    pub fn magnitude_squared(&self) -> N {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn unit(&self) -> Option<Vector<N>> {
        let magnitude = self.magnitude();
        if magnitude.is_zero() {
            None
        } else {
            Some(*self * (N::one() / magnitude))
        }
    }

    pub fn times(&self, times: N) -> Self {
        Vector {
            x: times * self.x,
            y: times * self.y,
            z: times * self.z,
        }
    }

    pub fn plus(&self, other: &Self) -> Self {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }

    pub fn minus(&self, other: &Self) -> Self {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }

    pub fn cross(&self, other: &Self) -> Self {
        Vector::with_xyz(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x,
        )
    }

    pub fn dot(&self, other: &Self) -> N {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub fn angle_closest_from_to(from: &Self, to: &Self) -> Radian<N> {
        Radian(N::atan2(from.cross(to).magnitude(), from.dot(to)))
    }

    pub fn angle_from_to_axis(from: &Self, to: &Self, axis: &Self) -> Radian<N> {
        let crossed = from.cross(to);
        let collinearity = crossed.dot(axis);
        Vector::angle_closest_from_to(from, to)
            * if collinearity.is_sign_positive() {
                N::one()
            } else {
                -N::one()
            }
    }

    pub fn lerp(from: &Self, to: &Self, t: N) -> Self {
        *from + (*to - *from) * t
    }

    pub fn negative(&self) -> Self {
        Vector::with_xyz(-self.x, -self.y, -self.z)
    }
}

impl<N> Mul<N> for Vector<N>
where
    N: Float,
{
    type Output = Self;
    fn mul(self, rhs: N) -> Self::Output {
        self.times(rhs)
    }
}

impl<N> Add<Vector<N>> for Vector<N>
where
    N: Float,
{
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        self.plus(&rhs)
    }
}

impl<N> Sub<Vector<N>> for Vector<N>
where
    N: Float,
{
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        self.minus(&rhs)
    }
}

impl<N> Neg for Vector<N>
where
    N: Float,
{
    type Output = Self;
    fn neg(self) -> Self::Output {
        self.negative()
    }
}

impl<N> From<(N, N, N)> for Vector<N> {
    fn from(source: (N, N, N)) -> Self {
        Self::with_xyz(source.0, source.1, source.2)
    }
}
