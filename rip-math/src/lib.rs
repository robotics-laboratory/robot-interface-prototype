#![no_std]

pub mod spatial;
pub mod unit;

pub mod util {
    pub fn wrap_value_from_to<N: num::Float>(value: N, from: N, to: N) -> N {
        let range = to - from;
        value - (range * N::floor((value - from) / range))
    }
}
