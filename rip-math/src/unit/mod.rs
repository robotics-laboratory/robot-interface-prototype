use core::time::Duration;

pub mod angular;

pub trait Integrate {
    type Output;
    fn integrate(self, duration: Duration) -> Self::Output;
}

pub trait Differentiate {
    type Output;
    fn differentiate(self, duration: Duration) -> Self::Output;
}
