use super::{Differentiate, Integrate};
use core::{
    ops::{Add, Mul, Neg, Sub},
    time::Duration,
};

#[derive(Clone, Copy, PartialOrd, PartialEq, Debug)]
pub struct Radian<N>(pub N);

pub const RAD: Radian<f64> = Radian(1.0);
pub const DEG: Radian<f64> = Radian(core::f64::consts::PI / 180.0);

impl<N> Mul<N> for Radian<N>
where
    N: Mul<Output = N>,
{
    type Output = Radian<N>;
    fn mul(self, rhs: N) -> Self::Output {
        Radian(self.0 * rhs)
    }
}

impl Mul<Radian<f64>> for f64 {
    type Output = Radian<f64>;
    fn mul(self, rhs: Radian<f64>) -> Self::Output {
        Radian(self * rhs.0)
    }
}

impl<N> Add for Radian<N>
where
    N: Add<Output = N>,
{
    type Output = Radian<N>;
    fn add(self, rhs: Radian<N>) -> Self::Output {
        Radian(self.0 + rhs.0)
    }
}

impl<N> Sub for Radian<N>
where
    N: Sub<Output = N>,
{
    type Output = Radian<N>;
    fn sub(self, rhs: Radian<N>) -> Self::Output {
        Radian(self.0 - rhs.0)
    }
}

impl<N> Neg for Radian<N>
where
    N: Neg<Output = N>,
{
    type Output = Radian<N>;
    fn neg(self) -> Self::Output {
        Radian(-self.0)
    }
}

#[derive(Clone, Copy, PartialOrd, PartialEq, Debug)]
pub struct RadianPerSecond<N>(pub N);

impl<N> Mul<N> for RadianPerSecond<N>
where
    N: Mul<Output = N>,
{
    type Output = RadianPerSecond<N>;
    fn mul(self, rhs: N) -> Self::Output {
        RadianPerSecond(self.0 * rhs)
    }
}

impl Mul<RadianPerSecond<f64>> for f64 {
    type Output = RadianPerSecond<f64>;
    fn mul(self, rhs: RadianPerSecond<f64>) -> Self::Output {
        RadianPerSecond(self * rhs.0)
    }
}

impl<N> Add for RadianPerSecond<N>
where
    N: Add<Output = N>,
{
    type Output = RadianPerSecond<N>;
    fn add(self, rhs: RadianPerSecond<N>) -> RadianPerSecond<N> {
        RadianPerSecond(self.0 + rhs.0)
    }
}

impl<N> Sub for RadianPerSecond<N>
where
    N: Sub<Output = N>,
{
    type Output = RadianPerSecond<N>;
    fn sub(self, rhs: RadianPerSecond<N>) -> RadianPerSecond<N> {
        RadianPerSecond(self.0 - rhs.0)
    }
}

impl<N> Neg for RadianPerSecond<N>
where
    N: Neg<Output = N>,
{
    type Output = RadianPerSecond<N>;
    fn neg(self) -> RadianPerSecond<N> {
        RadianPerSecond(-self.0)
    }
}

#[derive(Clone, Copy, PartialOrd, PartialEq, Debug)]
pub struct RadianPerSecondSquared<N>(pub N);

impl<N> Mul<N> for RadianPerSecondSquared<N>
where
    N: Mul<Output = N>,
{
    type Output = RadianPerSecondSquared<N>;
    fn mul(self, rhs: N) -> Self::Output {
        RadianPerSecondSquared(self.0 * rhs)
    }
}

impl Mul<RadianPerSecondSquared<f64>> for f64 {
    type Output = RadianPerSecondSquared<f64>;
    fn mul(self, rhs: RadianPerSecondSquared<f64>) -> Self::Output {
        RadianPerSecondSquared(self * rhs.0)
    }
}

impl<N> Add for RadianPerSecondSquared<N>
where
    N: Add<Output = N>,
{
    type Output = RadianPerSecondSquared<N>;
    fn add(self, rhs: RadianPerSecondSquared<N>) -> RadianPerSecondSquared<N> {
        RadianPerSecondSquared(self.0 + rhs.0)
    }
}

impl<N> Sub for RadianPerSecondSquared<N>
where
    N: Sub<Output = N>,
{
    type Output = RadianPerSecondSquared<N>;
    fn sub(self, rhs: RadianPerSecondSquared<N>) -> RadianPerSecondSquared<N> {
        RadianPerSecondSquared(self.0 - rhs.0)
    }
}

impl<N> Neg for RadianPerSecondSquared<N>
where
    N: Neg<Output = N>,
{
    type Output = RadianPerSecondSquared<N>;
    fn neg(self) -> RadianPerSecondSquared<N> {
        RadianPerSecondSquared(-self.0)
    }
}

impl Differentiate for Radian<f64> {
    type Output = RadianPerSecond<f64>;
    fn differentiate(self, duration: Duration) -> Self::Output {
        RadianPerSecond(self.0 / duration.as_secs_f64())
    }
}

impl Integrate for RadianPerSecond<f64> {
    type Output = Radian<f64>;
    fn integrate(self, duration: Duration) -> Self::Output {
        Radian(self.0 * duration.as_secs_f64())
    }
}

impl Differentiate for RadianPerSecond<f64> {
    type Output = RadianPerSecondSquared<f64>;
    fn differentiate(self, duration: Duration) -> Self::Output {
        RadianPerSecondSquared(self.0 / duration.as_secs_f64())
    }
}

impl Integrate for RadianPerSecondSquared<f64> {
    type Output = RadianPerSecond<f64>;
    fn integrate(self, duration: Duration) -> Self::Output {
        RadianPerSecond(self.0 * duration.as_secs_f64())
    }
}
