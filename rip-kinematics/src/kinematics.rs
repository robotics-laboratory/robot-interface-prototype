pub trait Kinematics {
    type Generalized;
    type Position;
    type IkSpecification;
    type FkSpecification;
}

pub trait Forward: Kinematics {
    type Error;
    fn forward(
        &self,
        generalized: &Self::Generalized,
        spec: &Self::FkSpecification,
    ) -> Result<(Self::Position, Self::IkSpecification), Self::Error>;
}

pub trait Inverse: Kinematics {
    type Error;
    fn inverse(
        &self,
        position: &Self::Position,
        spec: &Self::IkSpecification,
    ) -> Result<(Self::Generalized, Self::FkSpecification), Self::Error>;
}
