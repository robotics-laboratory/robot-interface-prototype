pub trait VelocityModel {
    type GeneralizedVelocity;
    type GeneralizedAcceleration;

    fn generalized_velocity(&self) -> Self::GeneralizedVelocity;
    fn generalized_acceleration(&self) -> Self::GeneralizedAcceleration;
}
